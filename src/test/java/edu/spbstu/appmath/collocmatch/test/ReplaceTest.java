package edu.spbstu.appmath.collocmatch.test;

import edu.spbstu.appmath.collocmatch.Match;
import edu.spbstu.appmath.collocmatch.MatchBank;
import edu.spbstu.appmath.collocmatch.Sentence;

import java.io.*;

/**
 * Created by Egor Gorbunov on 08.06.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */
public class ReplaceTest {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(new File("sentences/clear/sent_clear_1.txt")));
        MatchBank bank = new MatchBank();
        bank.read(br);
        br.close();

        BufferedWriter log = new BufferedWriter(new OutputStreamWriter(System.out));
        Match[] marr = bank.getAsArray(7);
        Match.writeMatch(log, marr[3]);
//        for (Match m : marr) {
//            Match.writeMatch(log, m);
//        }
        log.flush();


        Match m = marr[3];
        Sentence s = m.replaceCollocations("_COLLOCATION_");
        System.out.println(s.str());

    }
}
