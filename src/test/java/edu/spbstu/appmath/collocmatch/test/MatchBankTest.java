package edu.spbstu.appmath.collocmatch.test;

import edu.spbstu.appmath.collocmatch.CollocationDictionary;
import edu.spbstu.appmath.collocmatch.Match;
import edu.spbstu.appmath.collocmatch.MatchBank;

import java.io.*;

/**
 * Created by Egor Gorbunov on 07.06.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */
public class MatchBankTest {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(new File("sentences/clear/sent_clear_1.txt")));
        MatchBank bank = new MatchBank();
        bank.read(br);
        br.close();

        BufferedWriter log = new BufferedWriter(new OutputStreamWriter(System.out));
        Match[] marr = bank.getAsArray(3);
        for (Match m : marr) {
            Match.writeMatch(log, m);
        }
        log.flush();

        CollocationDictionary dict = bank.extractCollocationDictionary();
        System.out.println(dict.size());
    }
}
