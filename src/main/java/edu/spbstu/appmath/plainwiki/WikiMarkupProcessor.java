package edu.spbstu.appmath.plainwiki;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by Egor Gorbunov on 06.06.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */
public class WikiMarkupProcessor {
    private static final HashMap<String, Character> specialSymbolMap;
    private static final HashSet<String> stopHeadings;


    static {
        specialSymbolMap = new HashMap<>();
        specialSymbolMap.put("&nbsp;", ' ');

        stopHeadings = new HashSet<>();
        stopHeadings.add("References");
        stopHeadings.add("External link");
        stopHeadings.add("See also");
        stopHeadings.add("Notes");
        stopHeadings.add("Further reading");

    }

    private static class Data {
        int strPos;
        int cpyPos;

        public Data(int strPos, int cpyPos) {
            this.strPos = strPos;
            this.cpyPos = cpyPos;
        }
    }

    private static int getBracketEndPos(char[] str, int startPos) {
        int sum = 0;
        for (int i = startPos; i < str.length; ++i) {
            if (str[i] == '{')
                sum++;
            else if (str[i] == '}') {
                sum--;
            }
            if (sum == 0) {
                return i + 1;
            }
        }
        return -1;
    }

    /**
     * @return number of '=' signs at start
     */
    private static int getHeadingLen(char[] str, int startPos) {
        int sum = 0;
        for (int i = startPos; i < str.length; ++i) {
            if (str[i] == '=')
                sum += 1;
            else
                break;
        }
        return sum;
    }

    /**
     * @return index of first not space and not list character
     */
    private static int stripListItem(char[] str, int startPos) {
        for (int i = startPos; i < str.length; ++i) {
            if (str[i] != '*' && str[i] != ' ')
                return i;
        }
        return startPos;
    }

    /**
     * returns position, where tag (i.e. something like {@code <.../> or <...>...</...>}),
     * which starts at {@code startPos}, ends + 1.
     *
     * @param startPos -- must be index, for which {@code str[startPos] == '<'}
     */
    private static int getHtmlTagEndPos(char[] str, int startPos) {
        boolean isClosingTag = false;
        for (int i = startPos; i < str.length; ++i) {
            if (str[i] == '>' && isClosingTag) {
                return i + 1;
            }
            if ((str[i] == '/' && (i + 1 < str.length) && str[i + 1] == '>') ||
                    (str[i] == '<' && (i + 1 < str.length) && str[i + 1] == '/')) {
                isClosingTag = true;
            }
        }
        return startPos + 1;
    }

    private static int getXmlCommentEndPos(char[] str, int startPos) {
        for (int i = startPos; i < str.length; ++i) {
            if (str[i] == '-' && isEqualTo(str, i, "-->"))
                return i + 3;
        }
        return startPos + 1;
    }

    private static String getHeading(char[] str, int startPos) {
        int sum = 0;
        int sign = 1;
        int start = -1;
        int end = 0;
        for (int i = startPos; i < str.length; ++i) {
            if (str[i] == '=') {
                sum += sign;
            } else if (start < 0) {
                sign = -sign;
                end = i;
                start = i;
            } else {
                end++;
            }

            if (sum == 0) {
                return new String(str, start, end - start).trim();
            }


            if (str[i] == '\n' || str[i] == '\r')
                return "";
        }

        return "";
    }

    /**
     * Recursive function to process link: {@code [[ ... | ... |[[x|y]]z]] } will be changed to {@code yz}
     * @param str -- char array to process
     * @param startPos -- position to start from, {@code str[startPos]} must equal {@code '['}
     * @param cpyPos -- position, to copy good part of the string to, normally equal to {@code startPos} at first call
     * @return data with info about last processed element and position to copy next symbols...
     */
    private static Data clearLink(char[] str, int startPos, int cpyPos) {
        final int INNER_LINK_BRACKET_CNT = 4;

        int k = cpyPos;
        int bracketCnt = 0;
        for (int i = startPos; i < str.length; ++i) {

            switch (str[i]) {
                case '[':
                    bracketCnt += 1;
                    break;
                case ']':
                    bracketCnt -= 1;
                    break;
                case '|':
                    k = cpyPos;
                    break;
                default:
                    str[k++] = str[i];
            }

            if (bracketCnt == 0) {
                return new Data(i - 1, k);
            }

            if (bracketCnt == INNER_LINK_BRACKET_CNT) {
                Data d = clearLink(str, i - 1, k);
                i = d.strPos - 1;
                k = d.cpyPos;
            }
        }

        return new Data(str.length, k);
    }

    private static boolean isEqualTo(char[] str, int start, CharSequence s) {
        int sum = s.length();
        for (int i = start; i < str.length; ++i) {
            if (s.length() > i - start && sum > 0 && s.charAt(i - start) == str[i]) {
                sum -= 1;
            } else {
                break;
            }
        }
        return (sum == 0);
    }

    public static String cleanMarkup(String text) {
        char[] str = text.toCharArray();

        int cpyPos = 0;

        main_loop:
        for (int i = 0; i < str.length; ++i) {
            switch (str[i]) {
                case '[':
                    if (isEqualTo(str, i, "[[")) {
                        Data d = clearLink(str, i, cpyPos);
                        i = d.strPos + 1; // because of ]] and createLink return
                        cpyPos = d.cpyPos;
                    } else {
                        str[cpyPos++] = str[i];
                    }
                case '{':
                    i = getBracketEndPos(str, i) - 1;
                    break;
                case '<':
                    if (Character.isAlphabetic(str[i + 1]))
                        i = getHtmlTagEndPos(str, i) - 1;
                    else if (str[i + 1] == '!')
                        i = getXmlCommentEndPos(str, i);
                    break;
                case '&':
                    for (String key : specialSymbolMap.keySet()) {
                        if (isEqualTo(str, i, key)) {
                            str[cpyPos++] = specialSymbolMap.get(key);
                            i += key.length();
                        }
                    }
                    break;
                case '=':
                    if (stopHeadings.contains(getHeading(str, i))) {
                        break main_loop;
                    }
                default:
                    str[cpyPos++] = str[i];
            }

            if (i < 0)
                break;
        }

        return new String(str, 0, cpyPos);
    }
}
