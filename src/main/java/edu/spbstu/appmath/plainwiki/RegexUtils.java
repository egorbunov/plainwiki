package edu.spbstu.appmath.plainwiki;

import java.util.ArrayList;

/**
 * Created by Egor Gorbunov on 06.06.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */
public class RegexUtils {
    public static int getGroupCount(String pattern) {
        int cnt = 0;
        for (int i = 0; i < pattern.length(); ++i) {
            if (pattern.charAt(i) == '(')
                cnt += 1;
        }
        return cnt;
    }

    public static int getGroupIndex(String pattern, String groupPattern) {
        int ind = pattern.indexOf(groupPattern);
        int groupIndex = 0;
        for (int i = 0; i <= ind; ++i) {
            if (pattern.charAt(i) == '(')
                groupIndex += 1;
        }
        return groupIndex;
    }

    public static int[] getGroupIndices(String pattern, String groupPattern) {
        ArrayList<Integer> indices = new ArrayList<>();
        int start = 0;
        int groupIndex = 0;
        while (true) {
            int ind = pattern.indexOf(groupPattern, start);

            if (ind < 0)
                break;

            for (int i = start; i <= ind; ++i) {
                if (pattern.charAt(i) == '(')
                    groupIndex += 1;
            }
            indices.add(groupIndex);

            start = ind + 1;
        }

        int[] res = new int[indices.size()];
        for (int i = 0; i < indices.size(); ++i) {
            res[i] = indices.get(i);
        }
        return res;
    }

    public static String regexConcat(String... strs) {
        String resRegex = strs[0];
        for (int i = 1; i < strs.length; ++i) {
            resRegex += "|" + strs[i];
        }
        return resRegex;
    }
}
