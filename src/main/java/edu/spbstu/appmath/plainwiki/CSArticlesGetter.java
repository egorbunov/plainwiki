package edu.spbstu.appmath.plainwiki;

import net.sourceforge.jwbf.core.contentRep.Article;
import net.sourceforge.jwbf.mediawiki.bots.MediaWikiBot;
import org.json.simple.parser.ParseException;

import java.io.*;


/**
 * Created by Egor Gorbunov on 06.06.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */
public class CSArticlesGetter {
    static final private String OUT_FOLDER = "WIKI_ARTICLES/";
    static final private int MAX_ARTICLES_PER_FILE = 200;

    static final String[] WIKI_ARTICLES_IDS_JSON = {
            "wiki_articles_ids/d3_computerscience.json",
            "wiki_articles_ids/d5_algebra.json",
            "wiki_articles_ids/d5_computerprogramming.json",
            "wiki_articles_ids/d5_computing.json",
            "wiki_articles_ids/d5_math.json"
    };

    public static void main(String[] args) throws IOException, ParseException {
        for (String s : WIKI_ARTICLES_IDS_JSON) {
            System.out.println("PROCESSING FILE = " + s);

            Reader reader = new BufferedReader(new FileReader(new File(s)));

            String name = s.split("/")[1].split("\\.")[0];
            WArticle[] articles = CatScanJsonParser.parseArticles(reader);
            MediaWikiBot wikiBot = new MediaWikiBot("https://en.wikipedia.org/w/");

            int ind = 0;

            File file = new File(OUT_FOLDER + "text_" + name + "_" + ind + ".txt");
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
                    new File(OUT_FOLDER + "text_" + name + "_" + ind + ".txt")));

            int cnt = 0;
            for (WArticle doc : articles) {
                cnt += 1;

                if (cnt >= MAX_ARTICLES_PER_FILE) {

                    cnt = 0;
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    System.out.println("File generated: " + file.getName());

                    ind += 1;
                    file = new File(OUT_FOLDER + "text_" + name + "_" + ind + ".txt");
                    bufferedWriter = new BufferedWriter(new FileWriter(file));
                }

                Article article = wikiBot.getArticle(doc.title());

                try {
                    bufferedWriter.write(WikiMarkupProcessor.cleanMarkup(article.getText()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            bufferedWriter.flush();
            bufferedWriter.close();
        }
    }
}
