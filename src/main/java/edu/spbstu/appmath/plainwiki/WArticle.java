package edu.spbstu.appmath.plainwiki;

/**
 * Created by Egor Gorbunov on 06.06.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */
public class WArticle {
    private final String title;
    private final String id;

    public WArticle(String title, String id) {
        this.title = title;
        this.id = id;
    }

    public String title() {
        return title;
    }

    public String id() {
        return id;
    }
}
