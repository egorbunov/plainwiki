package edu.spbstu.appmath.plainwiki;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Egor Gorbunov on 06.06.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */

@Deprecated
public class WikiMarkupRegexProcessor {
    private static final boolean DEBUG = true;

    /**
     * That is synthetic label for regular expression, which must be placed in root regex group -- group,
     * with that all matched substring will be replaced
     */
    private static final String ROOT_STR = "ROOT";
    private static final String NO_LINKS_PATTERN = "((\\[\\[)([^\\]\\[]+[|])*(" + ROOT_STR + "[^\\]\\[]*)(\\]\\]))";
    private static final String NO_HTML_TAG_PATTERN = "((<TAG)([^<>]*)(/>))|((<TAG)([^<>]*[>])([^<>]*)(</TAG>))";
    private static final String NO_DOUBLE_BRACKETED_TEXT_PATTERN = "((\\{\\{)([^\\}\\{]*)(\\}\\}))";
    private static final String NO_HEADINGS_PATTERN = "(([=]+)(" + ROOT_STR + "[^=]+)([=]+))";


    private static final int[] maxPatterReplacementRules;
    private static final Pattern maxClearPattern;

    private static final int[] mediumPatterReplacementRules;
    private static final Pattern mediumClearPattern;

    static {
        // Warning: do not change order of parameters. NO_LINKS_PATTERN must be first because of all occurrences
        // are replaced with it's group at 4 position. And NO_HEADINGS_PATTERN must be after NO_LINKS_PATTERN =)

        String maxClearRegexWithRoots = RegexUtils.regexConcat(
                NO_LINKS_PATTERN,
                NO_HEADINGS_PATTERN.replace(ROOT_STR, ""),
                getRegexForTag("ref"),
                getRegexForTag("math"),
                NO_DOUBLE_BRACKETED_TEXT_PATTERN);

        maxPatterReplacementRules = RegexUtils.getGroupIndices(maxClearRegexWithRoots, ROOT_STR);
        maxClearPattern = Pattern.compile(maxClearRegexWithRoots.replace(ROOT_STR, ""));

        String mediumClearRegexWithRoots = RegexUtils.regexConcat(
                NO_LINKS_PATTERN,
                NO_HEADINGS_PATTERN,
                getRegexForTag("ref"),
                getRegexForTag("math"),
                NO_DOUBLE_BRACKETED_TEXT_PATTERN);

        mediumPatterReplacementRules = RegexUtils.getGroupIndices(mediumClearRegexWithRoots, ROOT_STR);
        mediumClearPattern = Pattern.compile(mediumClearRegexWithRoots.replace(ROOT_STR, ""));

    }

    private static String getRegexForTag(String tag) {
        return NO_HTML_TAG_PATTERN.replace("TAG", tag);
    }

    /**
     * @param text -- text to delete markup in
     * @param p -- patter, which MUST have {@code NO_LINKS_PATTERN} regex as first regex part
     *          (for correct group replacement)
     * @return text with some Wiki Markup stuff replace or/and deleted
     */
    private static String doClear(String text, Pattern p, int[] replacementRules) {
        Matcher m = maxClearPattern.matcher(text);
        StringBuffer sb = new StringBuffer(text.length());

        while (m.find()) {
            String toReplaceWith = "";
            for (int r : replacementRules) {
                if (m.group(r) != null) {
                    toReplaceWith = m.group(r);
                    break;
                }
            }

            if (DEBUG) {
                System.err.println("| " + m.group(0) + " | --> REPLACED WITH --> | " + toReplaceWith + " |");
            }

            m.appendReplacement(sb, Matcher.quoteReplacement(toReplaceWith));
        }
        m.appendTail(sb);
        return sb.toString();
    }

    /**
     * <li>Replace links [[%something%|%something%|%root value%]] with %root value%</li>
     * <li>Deletes {@code <ref>} tags</li>
     * <li>Deletes {@code <math>} tags</li>
     * <li>Deletes double-bracketed stuff link this:{@code {{Main|List of machine learning algorithms}} }</li>
     * <li>Deletes headings </li>
     */
    public static String clearMarkupMax(String text) {
        return doClear(text, maxClearPattern, maxPatterReplacementRules);
    }

    /**
     * <li>Replace links [[%something%|%something%|%root value%]] with %root value%</li>
     * <li>Deletes {@code <ref>} tags</li>
     * <li>Deletes {@code <math>} tags</li>
     * <li>Deletes double-bracketed stuff link this:{@code {{Main|List of machine learning algorithms}} }</li>
     * <li>Deletes {@code '='} signs around headings </li>
     */
    public static String clearMarkupMedium(String text) {
        return doClear(text, mediumClearPattern, mediumPatterReplacementRules);
    }

}
