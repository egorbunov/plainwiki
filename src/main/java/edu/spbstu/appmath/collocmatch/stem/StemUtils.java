package edu.spbstu.appmath.collocmatch.stem;

/**
 * Created by Egor Gorbunov on 07.06.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */
public class StemUtils {
    private static final String SEP_PATTERN = "[ \t]+";

    public static String stemAllWords(String s) {
        StringBuilder sb = new StringBuilder(s.length());
        String[] words = stemAllWordsSplit(s);
        for (String w : words) {
            sb.append(w).append(" ");
        }
        return sb.toString().trim();
    }

    public static String[] stemAllWordsSplit(String s) {
        String[] split = s.split(SEP_PATTERN);
        for (int i = 0; i < split.length; ++i) {
            split[i] = stemString(split[i]);
        }
        return split;
    }

    public static String stemString(String s) {
        Stemmer stemmer = new Stemmer();
        stemmer.add(s.toCharArray(), s.length());
        stemmer.stem();
        return stemmer.toString();
    }
}
