/**
 * Created by Egor Gorbunov on 07.06.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */

package edu.spbstu.appmath.collocmatch;

/**
 * Collocation is not actually a complete sentence, it much like a semantic unit
 * combined of more than one word, ex. Computer Science, Graph theory, Lie algebra, Windows XP,
 * Nonlinear function analysis etc.
 */
public class Collocation extends Sentence {

    /**
     * Index of word, which holds main sense of the collocation. Almost always that word is the last word
     * of collocation, so by default too.
     */
    private int kernelWordIndex;

    /**
     * Part of speech tag -- syntax role of this collocation. Almost always collocation is a noun, and
     * by default is treated to be so.
     */
    private String pos;

    private String gluedStr;

    public Collocation(String collocation) {
        super(collocation);

        // default values
        kernelWordIndex = words.length - 1;
        pos = "NN";

        StringBuilder sb = new StringBuilder();
        for (String w : words) {
            sb.append(w);
        }
        gluedStr = sb.toString();
    }

    public void setPOS(String pos) {
        this.pos = pos;
    }

    public String getPOS() {
        return pos;
    }

    public String getGluedStr() {
        return gluedStr;
    }
}
