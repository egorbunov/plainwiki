package edu.spbstu.appmath.collocmatch;

import edu.spbstu.appmath.collocmatch.utils.StringUtils;

import java.io.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Egor Gorbunov on 07.06.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */
public class MatchBank implements Iterable<Match> {
    private HashSet<Match> matches = null;

    /**
     * <p>
     *     Returns all collocations without repetitions, which were matched in sentences from
     *     this bank.
     * </p>
     * <p>
     *     Warning: every call would entails new collection of {@code Collocation} objects creation, so
     *     {@code CollocationDictionary} instances, which produced by that method are different.
     * </p>
     *
     */
    public CollocationDictionary extractCollocationDictionary() {
        HashSet<Collocation> collocations = new HashSet<>();

        List<MatchedCollocation> mcList;
        for (Match m : matches) {
            mcList = m.getMatchedCollocations();
            for (MatchedCollocation mc : mcList) {
                collocations.add(mc.collocation);
            }
        }
        CollocationDictionary dict = new CollocationDictionary(collocations);

        return dict;
    }

    public boolean addMatch(Match match) {
        if (matches == null) {
            matches = new HashSet<>();
        }

        return matches.add(match);
    }

    @Override
    public Iterator<Match> iterator() {
        return matches.iterator();
    }

    public Iterator<Sentence> sentenceIterator() {
        return new Iterator<Sentence>() {
            Iterator<Match> it = matches.iterator();
            @Override
            public boolean hasNext() {
                return it.hasNext();
            }

            @Override
            public Sentence next() {
                return it.next().getSentence();
            }
        };
    }

    public Match[] getAsArray() {
        return matches.toArray(new Match[matches.size()]);
    }

    public Match[] getAsArray(int minCollocationsMatch) {
        Match[] array = getAsArray();
        Arrays.sort(array, Match.BY_COLLOCATION_NUMBER);
        int ind = 0;
        for (int i = 0; i < array.length; ++i) {
            if (array[i].getMatchedCollocationNumber() < minCollocationsMatch || i == array.length - 1) {
                ind = i;
                break;
            }
        }
        return Arrays.copyOfRange(array, 0, ind);
    }

    public Sentence[] getSentenceArray() {
        Sentence[] sentences = new Sentence[matches.size()];
        int i = 0;
        for (Match m : matches) {
            sentences[i++] = m.getSentence();
        }
        return sentences;
    }

    /**
     * Dumps sentences to file with data required for successful restoration of
     * all collocations matched for that sentence
     * @param writer -- where sentences with collocations would be written
     * @param minCollocationMatchedNumber -- minimum number of collocations matched for sentences, that would be
     *                                    written
     */
    public void write(Writer writer, int minCollocationMatchedNumber) throws IOException {
        Match[] matchesArray = getAsArray();
        Arrays.sort(matchesArray, Match.BY_COLLOCATION_NUMBER);

        for (Match match : matchesArray) {
            if (match.getMatchedCollocationNumber() >= minCollocationMatchedNumber) {
                Match.writeMatch(writer, match);
            } else {
                break;
            }
        }
    }

    private String readLine(Reader reader) throws IOException {
        return readUntil(reader, '\n', 0);
    }

    private String readUntil(Reader reader, char stopChar, int skipAtStart) throws IOException {
        boolean eol = false;
        char additionalStopChar = '\r';
        if (stopChar == '\n') {
            eol = true;
        }

        StringBuilder sb = new StringBuilder();
        while (true) {
            int ci =  reader.read();
            if (ci < 0) {
                throw new EOFException();
            }
            char c = (char) ci;
            if (c == stopChar || (eol && c == additionalStopChar)) {
                if (eol && c == additionalStopChar) {
                    reader.read();
                }
                break;
            } else {
                if (skipAtStart > 0) {
                    skipAtStart -= 1;
                } else {
                    sb.append(c);
                }
            }
        }
        return sb.toString();
    }

    public void read(Reader reader) throws IOException {
        while (true) {
            try {
                // reading sentence
                String sentence = readLine(reader);

                Match match = new Match(new Sentence(StringUtils.deleteExtraSpacesAndSeparate(sentence)));

                // reading collocations
                int collocationsNumber = Integer.valueOf(readLine(reader));
                for (int i = 0; i < collocationsNumber; ++i) {
                    String collocationStr = readUntil(reader, '}', 1);
                    int from = Integer.valueOf(readUntil(reader, ',', 1));
                    int to = Integer.valueOf(readUntil(reader, ')', 0));

                    readUntil(reader, '\n', 0);

                    match.addCollocation(from, to, new Collocation(collocationStr));
                }

                addMatch(match);
            } catch (EOFException e) {
                break;
            }
        }
    }

    public int size() {
        return matches == null ? 0 : matches.size();
    }

    public void clear() {
        if (matches != null)
            matches.clear();
    }

}
