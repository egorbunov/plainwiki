package edu.spbstu.appmath.collocmatch;

/**
 * Created by Egor Gorbunov on 07.06.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */
public class MatchedCollocation {
    Collocation collocation;
    public int wStart;
    public int wEnd;
    public int start;
    public int end;

    /**
     * @param collocation -- collocation!
     * @param start -- start index in the string (inclusively)
     * @param end -- end index in the string (exclusively)
     * @param wStart -- start word index (inclusively)
     * @param wEnd -- end word index (inclusively!)
     */
    public MatchedCollocation(Collocation collocation, int start, int end, int wStart, int wEnd) {
        this.collocation = collocation;
        this.start = start;
        this.end = end;
        this.wStart = wStart;
        this.wEnd = wEnd;
    }

    public String getCollocationStr() {
        return collocation.str();
    }
}
