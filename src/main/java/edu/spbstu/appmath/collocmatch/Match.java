package edu.spbstu.appmath.collocmatch;

import edu.spbstu.appmath.collocmatch.utils.StringUtils;

import java.io.IOException;
import java.io.Writer;
import java.util.*;

/**
 * Created by Egor Gorbunov on 07.06.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */
public class Match {
    public int getMaxCollocationWordLen() {
        return maxCollocationWordLen;
    }

    private int maxCollocationWordLen = 0;

    public static void writeMatch(Writer writer, Match match) throws IOException {
        writer.write(match.getSentence().str());
        writer.write('\n');
        writer.write(Integer.toString(match.getMatchedCollocationNumber()));
        writer.write('\n');
        for (MatchedCollocation mc : match.getMatchedCollocations()) {
            writer.write("{" + mc.collocation.str() + "}" + "[" + mc.start + "," + mc.end +")");
            writer.write('\n');
        }
    }

    public static final Comparator<Match> BY_COLLOCATION_NUMBER = new Comparator<Match>() {
        @Override
        public int compare(Match o1, Match o2) {
            return - (o1.collocations.size() - o2.collocations.size());
        }
    };

    public static final Comparator<Match> BY_SENTENCE_LENGTH = new Comparator<Match>() {
        @Override
        public int compare(Match o1, Match o2) {
            return (o1.sentence.str().length() - o2.sentence.str().length());
        }
    };

    private final Sentence sentence;

    private final ArrayList<MatchedCollocation> collocations;

    private Sentence strippedSentence = null;

    public Match(Sentence sentence) {
        this.sentence = sentence;
        collocations = new ArrayList<>();
    }

    /**
     * @param from -- index, from which given collocation starts in sentence from current {@code Match}
     *             instance (inclusively)
     * @param to -- index in sentence, where given collocation ends (exclusively)
     * @param collocation -- collocation, that appeared in current sentence
     */
    public void addCollocation(int from, int to, Collocation collocation) {
        if (from > to || from < 0 || to < 1 || to > sentence.str().length())
            throw new IndexOutOfBoundsException("Bad bounds of collocation: from = " + from + ", to = " + to + "; " +
                    "Sentence length = " + sentence.str().length());

        collocations.add(new MatchedCollocation(collocation,
                from,
                to,
                sentence.getWordIndex(from),
                sentence.getWordIndex(to - 1)));

        if (collocation.words().length > maxCollocationWordLen) {
            maxCollocationWordLen = collocation.words().length;
        }
    }

    public List<MatchedCollocation> getMatchedCollocations() {
        return collocations;
    }

    @Override
    public int hashCode() {
        return sentence.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Match) {
            Match match = (Match) obj;
            return (this == match || match.getSentence().equals(sentence));
        }
        return false;
    }

    public Sentence getSentence() {
        return sentence;
    }

    public int getMatchedCollocationNumber() {
        return collocations.size();
    }

    public Sentence replaceCollocations(String word) {
        if (strippedSentence == null) {
            String[] words = Arrays.copyOf(sentence.words(), sentence.words().length);
            for (MatchedCollocation c : collocations) {
                for (int i = c.wStart; i < c.wEnd; ++i) {
                    words[i] = "";
                }
                words[c.wEnd] = word;
            }

            StringBuilder sb = new StringBuilder();
            for (String w : words) {
                sb.append(w).append(" ");
            }
            strippedSentence = new Sentence(StringUtils.deleteExtraSpacesAndSeparate(sb.toString().trim()));
        }
        return strippedSentence;
    }

    public String[] replaceCollocationsSplit(String word) {
        replaceCollocations(word);
        return strippedSentence.words();
    }

    /**
     * @return matched collocations from left to right through the sentence
     */
    public MatchedCollocation[] getSortedCollocations() {
        collocations.sort(new Comparator<MatchedCollocation>() {
            @Override
            public int compare(MatchedCollocation o1, MatchedCollocation o2) {
                return o1.start - o2.start;
            }
        });

        return collocations.toArray(new MatchedCollocation[collocations.size()]);
    }
}
