package edu.spbstu.appmath.collocmatch.search;

import java.util.ArrayList;
import java.util.List;

/**
 * Trie with suffix links for fast dictionary search
 *
 * T -- type for the data to store within Node in trie. Use it to get more info about match
 */
public class AhoTrie<T> {
    private boolean isCaseInsensitive;

    public class SearchResult {
        public T data;
        public int start; // inclusively
        public int end; // exclusively

        public SearchResult(T data, int start, int end) {
            this.data = data;
            this.start = start;
            this.end = end;
        }
    }

    private class Node {
        private static final int ALPHABET_SIZE = 96;
        private static final int OFFSET = 32;

        ArrayList<Node> next = null;

        Node parent;
        char ch;
        Node sLink;
        int isEnd;

        String endString;
        T data;

        public Node() {
            next = new ArrayList<>(ALPHABET_SIZE);

            for (int i = 0; i < ALPHABET_SIZE; ++i)
                next.add(null);

            isEnd = -1;
            sLink = null;
            parent = null;
            endString = null;
            ch = 0;
        }

        public Node getNext(char c) {
            return next.get(c - OFFSET);
        }

        public void setNext(char c, Node nd) {
            next.set(c - OFFSET, nd);
        }

    }

    private Node root;

    public AhoTrie(boolean isCaseInsensitive) {
        this.isCaseInsensitive = isCaseInsensitive;
        root = new Node();
    }

    public void addStr(String s, T data) {
        Node cur = root;
        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            if (isCaseInsensitive) {
                c = Character.toLowerCase(c);
            }
            if (cur.getNext(c) == null) {
                Node newNode = new Node();
                cur.setNext(c, newNode);
                newNode.parent = cur;
                newNode.ch = c;
                cur = newNode;
            } else {
                cur = cur.getNext(c);
            }
        }
        cur.data = data;
        cur.isEnd = 1;
        cur.endString = s;
    }

    private Node getNext(Node v, char c) {
        if (v.getNext(c) == null) {
            if (v == root)
                v.setNext(c, root);
            else
                v.setNext(c, getNext(getSuffix(v), c));
        }
        return v.getNext(c);
    }

    private Node getSuffix(Node v) {
        if (v.sLink == null) {
            if (v == root || v.parent == root) {
                v.sLink = root;
            } else {
                v.sLink = getNext(getSuffix(v.parent), v.ch);
            }
        }
        return v.sLink;
    }

    private Node getEnd(Node v) {
        if (v.isEnd == -1) {
            if (v == root)
                v.isEnd = 0;
            else {
                Node x = getEnd(getSuffix(v));
                v.isEnd = x.isEnd;
                v.endString = x.endString;
                v.data = x.data;
            }
        }
        return v;
    }

    /**
     * Returns {@code true} if given string contain any word from dictionary
     */
    public boolean doesStrContain(String str) {
        Node cur = root;
        for (int i = 0; i < str.length(); ++i) {
            char strChar = str.charAt(i);
            if (isCaseInsensitive) {
                strChar = Character.toLowerCase(strChar);
            }
            cur = getNext(cur, strChar);

            if (getEnd(cur).isEnd == 1) {
                return true;
            }

        }
        return false;
    }

    /**
     * Searches for first and longest match in given string, so consider that dictionary (trie) contains: [aba, baba],
     * and given string equal to: "ababa". Method will return "baba", not "aba", because it longer then "aba", but
     * if fiven string is: "aba baba" method will return "aba", because "baba" not align with "aba".
     */
    public SearchResult find(String str) {
        Node cur = root;

        SearchResult sr = null;

        for (int i = 0; i < str.length(); ++i) {
            char strChar = str.charAt(i);
            if (isCaseInsensitive) {
                strChar = Character.toLowerCase(strChar);
            }
            cur = getNext(cur, strChar);

            Node node = getEnd(cur);
            if (node.isEnd == 1) {
                int start = i - node.endString.length() + 1;
                int end = i + 1;

                // deleting space chars
                while (Character.isSpaceChar(str.charAt(start)))
                    start += 1;
                while (Character.isSpaceChar(str.charAt(end - 1)))
                    end -= 1;

                // renewing answer
                if (sr == null) {
                    sr = new SearchResult(node.data, start, end);
                } else {
                    if (start >= sr.end) {
                        return sr;
                    } else if (sr.end - sr.start < end - start){
                        sr.data = node.data;
                        sr.start = start;
                        sr.end = end;
                    }
                }
            }
        }
        return sr;
    }

    public List<SearchResult> findAll(String str) {
        ArrayList<SearchResult> list = null;

        int skipped = 0;
        SearchResult res = find(str);

        while (res != null) {
            if (list == null) {
                list = new ArrayList<>();
            }
            res.start += skipped;
            res.end += skipped;
            list.add(res);

            skipped = res.end;
            if (skipped < str.length()) {
                res = find(str.substring(skipped));
            } else {
                res = null;
            }
        }

        return list;
    }
}