package edu.spbstu.appmath.collocmatch;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Egor Gorbunov on 06.06.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */

/**
 * Very fast collocations finder in specified texts.
 */
public class CollocationMatch {
    private final static String[] collocationFilenames = {"collocations/c_algebra.txt", "collocations/c_computing.txt" };
    private final static boolean MAX_FILTERING = false;
    private final static boolean DO_STEMMED = true;

    private final static String ARTICLES_FOLDER = "WIKI_ARTICLES/";
    private final static String OUT_SENTENCE_FOLDER;
    private final static String SENT_FILENAME_PREFIX;
    private final static int MAX_SENT_IN_FILE = 1000;

    private final static Pattern strangeSymbolPattern = Pattern.compile("[^\\x20-\\x7F]");
    private final static Pattern notAlphaNumPatter = Pattern.compile("[^a-zA-Z ,?!\\.]");

    private final static Pattern sentFilteringPattern;

    private static int ind;

    static {
        if (MAX_FILTERING) {
            OUT_SENTENCE_FOLDER = "sentences/clear/";
            SENT_FILENAME_PREFIX = DO_STEMMED ? "sent_clear_stem_" : "sent_clear_";
            sentFilteringPattern = notAlphaNumPatter;
        } else {
            OUT_SENTENCE_FOLDER = "sentences/all/";
            SENT_FILENAME_PREFIX = DO_STEMMED ? "sent_stem_" : "sent_";
            sentFilteringPattern = strangeSymbolPattern;
        }

        File dir = new File(OUT_SENTENCE_FOLDER);
        ind = 0;
        try {
            for (File f : dir.listFiles()) {
                int i = f.getName().indexOf(SENT_FILENAME_PREFIX);
                int j = f.getName().indexOf(".");
                try {
                    int curInd = Integer.valueOf(f.getName().substring(i + SENT_FILENAME_PREFIX.length(), j));
                    if (curInd > ind) {
                        ind = curInd;
                    }
                } catch (Exception ignored) {
                }

            }
        } catch (NullPointerException e) {
            log("Exception: cannot list files in folder: " + OUT_SENTENCE_FOLDER);
        }
    }
    private static String nextFilename() {
        ind += 1;
        return OUT_SENTENCE_FOLDER + SENT_FILENAME_PREFIX + ind + ".txt";
    }


    private static void log(String msg) {
        System.out.println(msg);
    }

    private static class Stats {
        private static int MAX_COLLOCATION_NUMBER_IN_SENT = 15;

        static float totalMBProcessed = 0;
        static int totalFilesProcessed = 0;
        static int maxCollocationsInSentence = 0;
        static int totalMatchedSentenceNum = 0;
        static int totalSentenceNum = 0;
        static int maxMatchedCollocationWordNum = 0;

        static int[] sentenceCounts = new int[MAX_COLLOCATION_NUMBER_IN_SENT];

        public static String getStats() {
            String s = "Max collocations in one sentence = " + maxCollocationsInSentence + "\n"
                    + "Total matched sentence number = " + totalMatchedSentenceNum + "\n"
                    + "Total processed sentence number = " + totalSentenceNum + "\n"
                    + "Total number of files processed = " + totalFilesProcessed + "\n"
                    + "Total amount of text processed = " + totalMBProcessed + "(MB)" + "\n"
                    + "Max word num in matched collocation = " + maxMatchedCollocationWordNum + "\n";
            s += "Sentence counts with exact collocation number:" + "\n";
            for (int i = 0; i < maxCollocationsInSentence + 1; ++i) {
                s += "    Collocation num: " + i + " --> " + sentenceCounts[i] + "\n";
            }

            return s;
        }
    }

    public static void main(String[] args) throws IOException {
        long startTime = System.currentTimeMillis();

        CollocationDictionary dict = new CollocationDictionary(collocationFilenames);

        final int MAX_MATCHES_IN_BANK = 60000;

        MatchBank matchBank = new MatchBank();
        File dir = new File(ARTICLES_FOLDER);
        File[] directoryListing = dir.listFiles();


        for (File file : directoryListing != null ? directoryListing : new File[0]) {

            if (matchBank.size() > MAX_MATCHES_IN_BANK) {
                log("INFO: RENEWING MATCH BANK");
                writeBank(matchBank);
                matchBank.clear();
            }

            log("INFO: Processing file: " + file.getName());

            BufferedReader br = new BufferedReader(new FileReader(file));

            int cnt = 0;
            int matchedCnt = 0;
            while (true) {
                String line = br.readLine();
                if (line == null)
                    break;
                if (line.isEmpty())
                    continue;

                String[] sentences = splitToSentences(line);

                for (String s : sentences) {
                    cnt += 1;
                    if (sentFilteringPattern.matcher(s).find())
                        continue;
                    s = s.trim();
                    try {
                        Match match = (DO_STEMMED) ? (dict.searchAllStemmedCollocations(s)) : (dict.searchAllCollocations(s));
                        if (match != null) {
                            if (!matchBank.addMatch(match)) {
                                //log("INFO: Equal sentences detected.");
                            } else {
                                if (match.getMaxCollocationWordLen() > Stats.maxMatchedCollocationWordNum) {
                                    Stats.maxMatchedCollocationWordNum = match.getMaxCollocationWordLen();
                                }
                                Stats.totalMatchedSentenceNum += 1;
                                matchedCnt += 1;
                                if (match.getMatchedCollocationNumber() > Stats.maxCollocationsInSentence) {
                                    Stats.maxCollocationsInSentence = match.getMatchedCollocationNumber();
                                }
                                Stats.sentenceCounts[match.getMatchedCollocationNumber()] += 1;
                            }
                        }
                    } catch (ArrayIndexOutOfBoundsException e) {
                        log("ERROR: EXCEPTION IN TRIE (PROBABLY) !!!! SENTENCE = " + s);
                    }
                }
            }

            Stats.totalMBProcessed += file.length();
            Stats.totalFilesProcessed += 1;
            Stats.totalSentenceNum += cnt;
            log("Filtered sentences: " + matchedCnt + "/" + cnt);
        }
        Stats.totalMBProcessed /= (1 << 20);


        log("INFO: STATISTICS: \n" + Stats.getStats());

        writeBank(matchBank);

        float timeElapsed = (System.currentTimeMillis() - startTime) / 1000.0f;
        log("INFO: total time elapsed = " + timeElapsed + " seconds");
    }

    private static void writeBank(MatchBank matchBank) throws IOException {
        Match[] finalSentenceArray = matchBank.getAsArray();
        log("INFO: sorting sentence by matched collocation number");
        Arrays.sort(finalSentenceArray, Match.BY_COLLOCATION_NUMBER);
        log("INFO: Writing sentences to files");
        writeSentencesToFiles(finalSentenceArray);
    }

    private static void writeSentencesToFiles(Match[] matches) throws IOException {
        final int MIN_COLLOC_MATCH = 3;

        BufferedWriter bw = new BufferedWriter(new FileWriter(new File(nextFilename())));
        int writtenSentCnt = 0;

        for (Match match : matches) {

            if (match.getMatchedCollocationNumber() < MIN_COLLOC_MATCH) {
                bw.flush();
                bw.close();
                break;
            }

            Match.writeMatch(bw, match);

            writtenSentCnt += 1;

            if (writtenSentCnt > MAX_SENT_IN_FILE) {
                writtenSentCnt = 0;
                bw.flush();
                bw.close();
                bw = new BufferedWriter(new FileWriter(new File(nextFilename())));
            }

        }

    }

    private static String[] splitToSentences(String line) {
        final String END_PATTERN = "([.!?][ \t]+)|([.!?]$)";
        final Pattern p = Pattern.compile(END_PATTERN);

        LinkedList<String> strs = new LinkedList<>();

        Matcher m = p.matcher(line);
        int prev = 0;
        int next;
        while (m.find()) {
            next = m.start();
            strs.add(line.substring(prev, next + 1).trim());
            prev = next + 1;
        }

        return strs.toArray(new String[strs.size()]);
    }


}
