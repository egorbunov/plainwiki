package edu.spbstu.appmath.collocmatch.utils;

import java.util.Arrays;

/**
 * Created by Egor Gorbunov on 07.06.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */
public class StringUtils {
    public static boolean isPunctuation(char c) {
        return c == '.' || c == ',' || c == '!' || c == '?' || c == ';' || c == ':';
    }

    /**
     * Deletes extra spaces btw words and adds spaces between words and punctuation
     */
    public static String deleteExtraSpacesAndSeparate(String s) {
        char[] str = s.toCharArray();
        char[] newStr = new char[s.length() * 2];

        int cpyTo = 0;
        boolean prevSymIsSpace = true;
        for (int i = 0; i < str.length; ++i) {
            if (Character.isSpaceChar(str[i])) {
                if (!prevSymIsSpace) {
                    newStr[cpyTo++] = str[i];
                    prevSymIsSpace = true;
                }
            } else {
                prevSymIsSpace = false;
                if (isPunctuation(str[i]))
                    newStr[cpyTo++] = ' ';
                newStr[cpyTo++] = str[i];
            }
        }
        if (cpyTo > 1 && Character.isSpaceChar(newStr[cpyTo - 1]))
            cpyTo -= 1;
        return new String(newStr, 0, cpyTo);
    }

    public static String deleteExtraSpaces(String s) {
        char[] str = s.toCharArray();
        char[] newStr = new char[s.length()];

        int cpyTo = 0;
        boolean prevSymIsSpace = true;
        for (int i = 0; i < str.length; ++i) {
            if (Character.isSpaceChar(str[i])) {
                if (!prevSymIsSpace) {
                    newStr[cpyTo++] = str[i];
                    prevSymIsSpace = true;
                }
            } else {
                prevSymIsSpace = false;
                newStr[cpyTo++] = str[i];
            }
        }
        if (cpyTo > 1 && Character.isSpaceChar(newStr[cpyTo - 1]))
            cpyTo -= 1;
        return new String(newStr, 0, cpyTo);
    }
}
