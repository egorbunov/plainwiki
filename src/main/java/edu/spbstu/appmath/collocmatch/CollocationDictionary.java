package edu.spbstu.appmath.collocmatch;

import edu.spbstu.appmath.collocmatch.search.AhoTrie;
import edu.spbstu.appmath.collocmatch.stem.StemUtils;
import edu.spbstu.appmath.collocmatch.utils.StringUtils;

import java.io.*;
import java.util.*;

/**
 * Created by Egor Gorbunov on 06.06.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */
public class CollocationDictionary implements Iterable<Collocation> {
    private ArrayList<Collocation> collocations;

    private AhoTrie<Collocation> stemmedCollocationsTrie;
    private AhoTrie<Collocation> collocationsTrie;
    private HashMap<String, Integer> collocationWords;
    private HashMap<String, Integer> stemmedCollocationWords;

    private void incCnt(HashMap<String, Integer> counter, String s) {
        Integer cnt = counter.get(s);
        if (cnt == null) {
            counter.put(s, 1);
        } else {
            counter.put(s, cnt + 1);
        }
    }

    /**
     * That method initializes all structures that used for search in dictionary.
     * If must be called after {@code collocations} filed is renewed or changed...
     */
    private void initialize() {
        collocationsTrie = new AhoTrie<>(true);
        stemmedCollocationsTrie = new AhoTrie<>(true);

        collocationWords = new HashMap<>();
        stemmedCollocationWords = new HashMap<>();

        for (Collocation collocation : collocations) {
            for (int k = 0; k < collocation.words().length; ++k) {

                incCnt(collocationWords, collocation.words[k]);
                incCnt(stemmedCollocationWords, collocation.stemmedWords[k]);
            }

            collocationsTrie.addStr(collocation.str(), collocation);
            stemmedCollocationsTrie.addStr(collocation.stemmed(), collocation);
        }
    }

    public CollocationDictionary(String[] filenames) throws IOException {
        collocations = new ArrayList<>();

        for (String filename : filenames) {
            BufferedReader br = new BufferedReader(new FileReader(new File(filename)));

            while (true) {
                String s = br.readLine();
                if (s == null)
                    break;
                collocations.add(new Collocation(s.trim().toLowerCase()));
            }
        }

        initialize();
    }

    public CollocationDictionary(Collocation[] inCollocations) {
        collocations = new ArrayList<>(Arrays.asList(inCollocations));
        initialize();
    }

    public CollocationDictionary(Collection<Collocation> inCollocations) {
        collocations = new ArrayList<>(inCollocations);
        initialize();
    }

    public boolean isWordFromCollocation(String word) {
        return collocationWords.containsKey(word);
    }

    public boolean isWordFromCollocationStemmed(String word) {
        String stemmed = StemUtils.stemString(word);
        return stemmedCollocationWords.containsKey(stemmed);
    }

    /**
     * Returns {@code true} if given string contains any collocation from dictionary
     */
    public boolean doesContainCollocation(String s) {
        return collocationsTrie.doesStrContain(s);
    }

    /**
     * @param s -- stemmed string!
     */
    public boolean doesContainStemmedCollocation(String s) {
        return stemmedCollocationsTrie.doesStrContain(s);
    }

    public Match searchAllCollocations(String str) {
        str = StringUtils.deleteExtraSpacesAndSeparate(str);
        List<AhoTrie<Collocation>.SearchResult> resultList = collocationsTrie.findAll(str);
        if (resultList == null) {
            return null;
        } else {
            Match match = new Match(new Sentence(StringUtils.deleteExtraSpacesAndSeparate(str)));
            for (AhoTrie<Collocation>.SearchResult sr : resultList) {
                match.addCollocation(sr.start, sr.end, sr.data);
            }
            return match;
        }
    }

    public Match searchAllStemmedCollocations(String str) {
        Sentence sentence = new Sentence(StringUtils.deleteExtraSpacesAndSeparate(str));
        List<AhoTrie<Collocation>.SearchResult> resultList = stemmedCollocationsTrie.findAll(sentence.stemmed());
        if (resultList == null) {
            return null;
        } else {
            Match match = new Match(sentence);
            for (AhoTrie<Collocation>.SearchResult sr : resultList) {
                match.addCollocation(sr.start, sr.end, sr.data);
            }
            return match;
        }
    }

    public boolean isInDictionary(Collocation collocation) {
        AhoTrie<Collocation>.SearchResult searchResult = collocationsTrie.find(collocation.str.trim()); // TODO: trim is maybe redundant

        return (searchResult != null) && (searchResult.end - searchResult.start == collocation.str().length());
    }

    public float scoreCollocation(Collocation collocation) {
        float score = 1.0f;

        // EGOR_INFO: there is some hard code for now; TODO: change
        if (!collocation.getPOS().equals("NN")) {
            return 0f;
        }

        if (isInDictionary(collocation))
            return score;

        for (String w : collocation.words()) {
            float cnt = collocationWords.get(w) == null ? 0 : collocationWords.get(w);
            score *= cnt / collocationWords.size();
        }
        score *= collocation.words().length;

        return score;
    }

    @Override
    public Iterator<Collocation> iterator() {
        return collocations.iterator();
    }

    public int size() {
        return collocations.size();
    }
}
