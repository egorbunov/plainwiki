package edu.spbstu.appmath.collocmatch;

import edu.spbstu.appmath.collocmatch.stem.StemUtils;
import edu.spbstu.appmath.collocmatch.utils.StringUtils;

/**
 * Created by Egor Gorbunov on 07.06.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */
public class Sentence {
    protected String str;
    protected String stemmedStr;
    protected String[] words;
    protected String[] stemmedWords;

    public Sentence(String sentence) {
        str = sentence;
        words = str.split("[ \t]+");
        stemmedWords = StemUtils.stemAllWordsSplit(str);
        stemmedStr = StemUtils.stemAllWords(str);
    }


    public String str() {
        return str;
    }

    public String stemmed() {
        return stemmedStr;
    }

    public String[] words() {
        return words;
    }


    public String[] stemmedWords() {
        return stemmedWords;
    }

    @Override
    public int hashCode() {
        return str.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Sentence) {
            Sentence sentence = (Sentence) obj;
            return (this == sentence || str.equals(sentence.str));
        }
        return false;
    }

    @Override
    public String toString() {
        return str;
    }

    /**
     * @param index -- index of specific char in sentence string (as it returned by {@code str()} method
     * @return index of the word, which holds given char index
     */
    public int getWordIndex(int index) {
        int len = 0;
        for (int i = 0; i < words.length; ++i) {
            len += words[i].length();
            if (index < len) {
                return i;
            }
            len += 1;
        }
        return 0;
    }


    /**
     * same as {@code getWordIndex(int)}, but searches in stemmed string
     */
    public int getWordIndexInStemmedStr(int index) {
        int len = 0;
        for (int i = 0; i < stemmedWords.length; ++i) {
            len += stemmedWords[i].length();
            if (index < len) {
                return i;
            }
            len += 1;
        }
        return 0;
    }

    public int wordNum() {
        return words.length;
    }

    public int length() {
        return str.length();
    }
}
